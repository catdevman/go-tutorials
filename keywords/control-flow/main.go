package main

import "fmt"

func main() {

	// if checks the conditional (in this case "true") is true
	// otherwise it continues to go down until it hits else
	// you can have additional branches (more ifs) by using "else if" with a conditional
	// the logic is checked in order so multiple could be true but just the first branch that equals true will be ran
	if true {
		fmt.Println("True first if")
	} else {
		fmt.Println("False first if")
	}

	if !true {
		fmt.Println("True second if")
	} else {
		fmt.Println("False second if")
	}
}
