package main

import (
	"fmt"

	catmath "gitlab.com/catdevman/go-tutorials/keywords/declaration/math"
)

// GLOBALS
// safe
const notExported = 10.0
const Exported = "Just a string"

// unsafe
var v1 float64 = notExported

//END GLOBALS

type thing struct {
	one string
	two int
}

// func main is a special function in the main package that gets called as the entrypoint into your appliaction
func main() {
	v1 = 50.0
	fmt.Println(v1)
	var v2 = thing{
		"hello",
		two(),
	}
	fmt.Printf("%f\n", v1)
	fmt.Println(v1, v2.one, v2.two)
	fmt.Println(catmath.PI)
	// fmt.Println(catmath.py) // This is not exported and will not work. Uncomment it is see the error
}

func two() int {
	// danger of global state variables is that they can be changed from anywhere
	v1 = 2.0 // please don't do this in your code it is purely for example
	return int(v1)
}
